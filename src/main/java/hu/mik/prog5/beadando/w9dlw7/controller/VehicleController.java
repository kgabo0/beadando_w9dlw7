package hu.mik.prog5.beadando.w9dlw7.controller;

import hu.mik.prog5.beadando.w9dlw7.dto.vehicle.VehicleDTO;
import hu.mik.prog5.beadando.w9dlw7.dto.vehicle.VehicleTypeDTO;
import hu.mik.prog5.beadando.w9dlw7.service.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "api", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class VehicleController {

    private final VehicleService vehicleService;

    @GetMapping(value = "vehicles", consumes = MediaType.ALL_VALUE)
    public List<VehicleDTO> listVehicles(){
        return this.vehicleService.findAll();
    }

    @GetMapping("vehicles/types")
    public VehicleTypeDTO[] getTypeName(){
        return this.vehicleService.getTypes();
    }

    @GetMapping(value = "vehicles/{id}", consumes = MediaType.ALL_VALUE)
    public Optional<VehicleDTO> findById(@PathVariable("id") Long id){
        return this.vehicleService.findById(id);
    }

    @PutMapping(value = "vehicles", consumes = MediaType.ALL_VALUE)
    @PreAuthorize("hasAnyAuthority('ADMIN', 'WORKER')")
    @ResponseStatus(HttpStatus.CREATED)
    public VehicleDTO save(@RequestBody VehicleDTO vehicleDTO){
        return this.vehicleService.save(vehicleDTO);
    }

    @DeleteMapping(value = "vehicles/{id}", consumes = MediaType.ALL_VALUE)
    @PreAuthorize("hasAuthority('ADMIN')")
    public void delete(@PathVariable("id") Long id){
        this.vehicleService.delete(id);
    }

}
