package hu.mik.prog5.beadando.w9dlw7.service;

import hu.mik.prog5.beadando.w9dlw7.dto.vehicle.VehicleDTO;
import hu.mik.prog5.beadando.w9dlw7.dto.vehicle.VehicleTypeDTO;
import hu.mik.prog5.beadando.w9dlw7.mapper.vehicle.VehicleMapper;
import hu.mik.prog5.beadando.w9dlw7.repository.VehicleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class VehicleService {

    private final VehicleRepository vehicleRepository;

    private final VehicleMapper vehicleMapper;

    public List<VehicleDTO> findAll(){
        return this.vehicleRepository.findAllByOrderByIdAsc().stream().map(this.vehicleMapper::mapToVehicleDto).toList();
    }

    public VehicleDTO save(VehicleDTO vehicleDTO){
        return this.vehicleMapper.mapToVehicleDto(this.vehicleRepository.save(this.vehicleMapper.mapToVehicle(vehicleDTO)));
    }

    public Optional<VehicleDTO> findById(Long id){
        return this.vehicleRepository.findById(id).map(this.vehicleMapper::mapToVehicleDto);
    }

    public void delete(Long id){
        this.vehicleRepository.deleteById(id);
    }

    public VehicleTypeDTO[] getTypes(){
        return VehicleTypeDTO.values();
    }

}
