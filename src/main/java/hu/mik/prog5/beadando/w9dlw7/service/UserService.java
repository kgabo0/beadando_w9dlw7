package hu.mik.prog5.beadando.w9dlw7.service;

import hu.mik.prog5.beadando.w9dlw7.dto.user.SafeUserDTO;
import hu.mik.prog5.beadando.w9dlw7.mapper.user.UserMapper;
import hu.mik.prog5.beadando.w9dlw7.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    public List<SafeUserDTO> findAll(){
        return this.userRepository.findAll().stream().map(this.userMapper::mapToUserDto).toList();
    }

}
