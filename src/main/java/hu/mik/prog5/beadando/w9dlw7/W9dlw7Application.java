package hu.mik.prog5.beadando.w9dlw7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class W9dlw7Application {

	public static void main(String[] args) {
		SpringApplication.run(W9dlw7Application.class, args);
	}

}
