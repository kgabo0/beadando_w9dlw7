package hu.mik.prog5.beadando.w9dlw7.dto.vehicle;

public enum VehicleTypeDTO {

    Truck, Car, Sports_Car;

}
