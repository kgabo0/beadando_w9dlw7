package hu.mik.prog5.beadando.w9dlw7.controller;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WebController {

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String showHome(){
        return "home";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, consumes = MediaType.ALL_VALUE)
    public String showVehicles() {
        return "list";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN')")
    public String showUsers(){
        return "users";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET, consumes = MediaType.ALL_VALUE)
    public String showNewVehicle() {
        return "new";
    }

}
