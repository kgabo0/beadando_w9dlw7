package hu.mik.prog5.beadando.w9dlw7.controller;

import hu.mik.prog5.beadando.w9dlw7.dto.user.SafeUserDTO;
import hu.mik.prog5.beadando.w9dlw7.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "api", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;

    @GetMapping(value = "users", consumes = MediaType.ALL_VALUE)
    public List<SafeUserDTO> listAll(){
        return this.userService.findAll();
    }

}
