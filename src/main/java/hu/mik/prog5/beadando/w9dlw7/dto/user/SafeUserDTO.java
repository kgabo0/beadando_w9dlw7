package hu.mik.prog5.beadando.w9dlw7.dto.user;

import lombok.Data;

@Data
public class SafeUserDTO {

    private Long id;
    private String username;

}
