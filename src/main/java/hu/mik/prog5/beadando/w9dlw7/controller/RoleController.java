package hu.mik.prog5.beadando.w9dlw7.controller;

import hu.mik.prog5.beadando.w9dlw7.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "api", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoleController {

    private final RoleService securityService;

    @GetMapping("role-list")
    public List<String> getRoles(){
        return this.securityService.getRoles();
    }

}
