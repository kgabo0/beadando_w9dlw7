CREATE ROLE beadando_web WITH
    LOGIN
    SUPERUSER
    CREATEDB
    CREATEROLE
    INHERIT
    REPLICATION
    CONNECTION LIMIT -1
    PASSWORD '12345678';

CREATE TABLE users(
                      id    serial primary key,
                      username  varchar(35) not null unique,
                      password  varchar(100) not null
);

CREATE TABLE roles(
                      id    serial primary key,
                      role_name varchar(25)
);

CREATE TABLE user_role(
                          id    serial primary key,
                          user_id   serial not null references users(id),
                          role_id   serial not null references roles(id)
);

CREATE TABLE vehicles(
                         id    serial primary key,
                         name  varchar(100) not null,
                         price int not null,
                         type  varchar(25) not null
);

/*-----------------------------------------------------*/

insert into users(username, password)
values ('admin', '$2a$12$wh8MTwBmUT.OntRGB5iFVOeNQIF7UD7Uf49p9rsxbs2cp/npCD71i'); --password: admin

insert into users(username, password)
values ('customer', '$2a$12$EtqidOz7jZc5O4p9O2MiEO2T3rmC/pflKz0B1uk3ynE2MTVYphAgO'); --password: customer

insert into users(username, password)
values ('worker', '$2a$12$YkblRDcNh6324f8caFyRnORY8YQ0PrTlfaZd/0LMmZ45xG7A9y0ze'); --password: worker

/*-----------------------------------------------------*/

insert into vehicles(name, price, type)
values ('Scania R620', 65000, 'Truck');

insert into vehicles(name, price, type)
values ('Porsche 992 GT3RS', 240000, 'Sports_Car');

insert into vehicles(name, price, type)
values ('Alfa Romeo Giulia QV', 70000, 'Car');

/*-----------------------------------------------------*/

insert into roles(role_name)
values ('ADMIN');

insert into roles(role_name)
values ('CUSTOMER');

insert into roles(role_name)
values ('WORKER');

/*-----------------------------------------------------*/

insert into user_role(user_id, role_id)
select *
from(select u.id from users u where u.username = 'admin') a,
    (select r.id from roles r where r.role_name = 'ADMIN') b;

insert into user_role(user_id, role_id)
select *
from(select u.id from users u where u.username = 'admin') a,
    (select r.id from roles r where r.role_name = 'WORKER') b;

insert into user_role(user_id, role_id)
select *
from(select u.id from users u where u.username = 'worker') a,
    (select r.id from roles r where r.role_name = 'WORKER') b;

insert into user_role(user_id, role_id)
select *
from(select u.id from users u where u.username = 'customer') a,
    (select r.id from roles r where r.role_name = 'CUSTOMER') b;
