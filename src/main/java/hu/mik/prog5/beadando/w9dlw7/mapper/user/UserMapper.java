package hu.mik.prog5.beadando.w9dlw7.mapper.user;

import hu.mik.prog5.beadando.w9dlw7.dto.user.SafeUserDTO;
import hu.mik.prog5.beadando.w9dlw7.entity.User;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface UserMapper {

    SafeUserDTO mapToUserDto(User user);

    User mapToUser(SafeUserDTO safeUserDTO);

}
