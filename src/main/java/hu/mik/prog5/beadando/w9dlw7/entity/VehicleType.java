package hu.mik.prog5.beadando.w9dlw7.entity;

public enum VehicleType {

    Truck, Car, Sports_Car;

}
