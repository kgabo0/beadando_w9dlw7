# Járműkereskedés nyilvántartó rendszer
REST keretrendszer segítségével elkészített rendszer, amely a megjelenítéshez Thymeleaf és Vue.js implementációt valósít meg.

A security konfiguráció Spring Security által van végrehajtva. Az adatbázis elérésről PostgreSQL gondoskodik, amely Docker segítségével lett konfigurálva.

## Funkciók
Az alkalmazás 3 userrel rendelkezik: admin, worker és customer. Ezen 3 user eltérő szerepkörökkel illetve engedélyekkel rendelkezik.
- Admin user 2 jogosultsággal rendelkezik: ADMIN és WORKER. Admin felhasználóval lehetőségünk van userek listázására, nyilvántartás megtekintésére, nyilvántartásban szereplő elemek módosítására és törlésére, valamint új elem hozzáadására.
- Worker user 1 jogosultsággal rendelkezik: WORKER. A worker felhasználó megtekintheti a nyilvántartást, módosíthatja az elemeit, de nem törölheti azokat. Worker is létrehozhat új elemet.
- Customer user 1 jogosultsággal rendelkezik: CUSTOMER. Customer felhasználónak csupán 1 funkció érhető el, a nyilvántartás megtekintése.



