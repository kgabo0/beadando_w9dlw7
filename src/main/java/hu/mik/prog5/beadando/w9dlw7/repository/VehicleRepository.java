package hu.mik.prog5.beadando.w9dlw7.repository;

import hu.mik.prog5.beadando.w9dlw7.entity.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    List<Vehicle> findAllByOrderByIdAsc();

}
