package hu.mik.prog5.beadando.w9dlw7.repository;

import hu.mik.prog5.beadando.w9dlw7.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

}
