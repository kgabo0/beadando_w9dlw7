package hu.mik.prog5.beadando.w9dlw7.dto.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class VehicleDTO {

    private Long id;
    private String name;
    private Long price;

    @JsonProperty("vehicleType")
    private VehicleTypeDTO vehicleType;

}
