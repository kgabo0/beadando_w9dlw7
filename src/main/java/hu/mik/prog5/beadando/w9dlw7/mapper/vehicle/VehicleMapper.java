package hu.mik.prog5.beadando.w9dlw7.mapper.vehicle;

import hu.mik.prog5.beadando.w9dlw7.dto.vehicle.VehicleDTO;
import hu.mik.prog5.beadando.w9dlw7.dto.vehicle.VehicleTypeDTO;
import hu.mik.prog5.beadando.w9dlw7.entity.Vehicle;
import hu.mik.prog5.beadando.w9dlw7.entity.VehicleType;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface VehicleMapper {

    VehicleDTO mapToVehicleDto(Vehicle vehicle);

    Vehicle mapToVehicle(VehicleDTO vehicleDTO);

    VehicleTypeDTO mapToVehicleTypeDto(VehicleType vehicleType);

    VehicleType mapToVehicleType(VehicleTypeDTO vehicleTypeDTO);

}
